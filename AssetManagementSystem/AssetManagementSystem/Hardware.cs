using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagementSystem
{
  class Hardware:IAsset
  {
    public string HardwareName { get; set; }

    public int HardwareID { get; set; }

    Hardware hardware;

    List<Hardware> hardwares = new List<Hardware>();
    public void Add()
    {
      hardware = new Hardware();
      Console.WriteLine("Enter Hardware ID");
      hardware.HardwareID = Convert.ToInt32(Console.ReadLine());
      Console.WriteLine("Enter Hardware Name");
      hardware.HardwareName = Console.ReadLine();

      hardwares.Add(hardware);

    }

    public void Delete()
    {
      int index;
      Console.WriteLine("Enter the index");
      index = Convert.ToInt32(Console.ReadLine());
      hardwares.RemoveAt(index);

    }

    public void ListAll()
    {
      foreach (var item in hardwares)
      {
        Console.WriteLine("The hardware id is" + item.HardwareID);
        Console.WriteLine("The hardware name is  " + item.HardwareName);
      }
    }

    public void Search()
    {
      int id;
      int check = 0;
      Hardware bookTemp = null;
      Console.WriteLine("Enter the hardware id you want to search");
      id = Convert.ToInt32(Console.ReadLine());

      foreach (var item in hardwares)
      {
        if (hardware.HardwareID == id)
        {
          check = 1;
          bookTemp = item;
        }
      }
      if (check == 1)
      {
        Console.WriteLine("Record Found");
        Console.WriteLine("HardwareId" + bookTemp.HardwareID);
        Console.WriteLine("HardwareName" + bookTemp.HardwareName);

      }
      else
        Console.WriteLine("Record not found");


    }

    public void Update()
    {
      throw new NotImplementedException();
    }
  }
}
