using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssetManagementSystem
{
 public interface IAsset
  {
    void Add();
    void Search();
    void Delete();
    void Update();
    void ListAll();

    

  }
}
